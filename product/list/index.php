<?php
	class product{
		private $sku;
		private $name;
		private $price;

		//Reads the basic data about the product from a row returned by SQL query
		public function readDB($row){
			$this->sku = $row[0];
			$this->name = $row[1];
			$this->price = $row[2];
		}

		//Prints the basic data into the HTML
		public function print(){
			echo "<div class=\"product\">";
			echo "<input class=\"check\" type=\"checkbox\" name=\"checkList[]\" value=\"".$this->sku."\">";
			echo "<p>SKU: ".$this->sku."</p>";
			echo "<p>Name: ".$this->name."</p>";
			echo "<p>Price: ".$this->price."</p>";
		}
	}

	class dvd extends product{
		private $size;

		//Reads the DVD size from a row returned by SQL query
		public function readSize($row){
			$this->size = $row[3];
		}

		//Prints the DVD size into the HTML
		public function printSize(){
			echo "<p>Type: DVD</p>";
			echo "<p>Size: ".$this->size."</p>";
			echo "</div>";
		}
	}

	class book extends product{
		private $weight;

		//Reads the book's weight from a row returned by SQL query
		public function readWeight($row){
			$this->weight = $row[3];
		}

		//Prints the book's weight into the HTML
		public function printWeight(){
			echo "<p>Type: Book</p>";
			echo "<p>Weight: ".$this->weight."</p>";
			echo "</div>";
		}
	}

	class furniture extends product{
		private $height;
		private $width;
		private $length;

		//Reads the furniture's dimensions from a row returned by SQL query
		public function readDimensions($row){
			$this->height = $row[3];
			$this->width = $row[4];
			$this->length = $row[5];
		}

		//Prints the furniture's dimensions into the HTML
		public function printDimensions(){
			echo "<p>Type: Furniture</p>";
			echo "<p>Dimensions: ".$this->height."x".$this->width."x".$this->length."</p>";
			echo "</div>";
		}
	}

	//Deletes the products marked with a checkbox
	if($_SERVER["REQUEST_METHOD"] == "POST"){
		$mysqli = new mysqli('127.0.0.1', 'root', '', '');
		if ($mysqli->connect_error) {
    		die('Connect Error (' . $mysqli->connect_errno . ') '. $mysqli->connect_error);
		}
		$mysqli->select_db("Products");

		if(!empty($_POST['checkList'])){
			foreach($_POST['checkList'] as $check){
				$check = $mysqli->real_escape_string($check);
				$query = "DELETE FROM Product WHERE SKU = '".$check."'";
				$mysqli->query($query);
				$query = "DELETE FROM Size WHERE Product_SKU = '".$check."'";
				$mysqli->query($query);
				$query = "DELETE FROM Weight WHERE Product_SKU = '".$check."'";
				$mysqli->query($query);
				$query = "DELETE FROM Dimensions WHERE Product_SKU = '".$check."'";
				$mysqli->query($query);
			}
		}
		$mysqli->close();
	}

?>

<!DOCTYPE html>
<html lang="lv">
	<head>
		<title>Product List</title>
		<meta charset="UTF-8">
		<script src="/script.js"></script>
		<link rel="stylesheet" type="text/css" href="/style.css">
	</head>
	<body>
		<div id="form">
			<form name="action" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
			<header>
			<div class="head">
				<h1>Product List</h1>
			</div>
			<div id="right">
				<select class="head" name="action">
					<option value="1">Mass Delete Action</option>
				</select>
				<br>
				<input class="head" type="submit" value="Apply">
			</div>
			</header>
				<?php
					//Prints all products
					$mysqli = new mysqli('127.0.0.1', 'root', '', '');
					if ($mysqli->connect_error) {
    					die('Connect Error (' . $mysqli->connect_errno . ') '. $mysqli->connect_error);
					}
					$mysqli->select_db("Products");
					//Prints DVD's
					$query = "SELECT SKU, Name, Price, Size FROM Product, Size WHERE SKU = Product_SKU";
					$result = $mysqli->query($query);
					while($row = $result->fetch_array(MYSQLI_NUM)){
						$product = new dvd;
						$product->readDB($row);
						$product->readSize($row);
						$product->print();
						$product->printSize();
					}
					//Prints Books
					$query = "SELECT SKU, Name, Price, Weight FROM Product, Weight WHERE SKU = Product_SKU";
					$result = $mysqli->query($query);
					while($row = $result->fetch_array(MYSQLI_NUM)){
						$product = new book;
						$product->readDB($row);
						$product->readWeight($row);
						$product->print();
						$product->printWeight();
					}
					//Prints Furniture
					$query = "SELECT SKU, Name, Price, Height, Width, Length FROM Product, Dimensions WHERE SKU = Product_SKU";
					$result = $mysqli->query($query);
					while($row = $result->fetch_array(MYSQLI_NUM)){
						$product = new furniture;
						$product->readDB($row);
						$product->readDimensions($row);
						$product->print();
						$product->printDimensions();
					}
					$mysqli->close();
				?>
			</form>
		</div>
		<button type="button" onclick="productAddView()">Add a product</button>		
	</body>
</html>