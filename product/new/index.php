<?php
	class product{
		private $sku;
		private $name;
		private $price;

		//Protection against code injection
		public function testInput($data){
			$data = trim($data);
			$data = stripslashes($data);
			$data = htmlspecialchars($data);
			return $data;
		}

		//Reads the basic product values from the form
		public function setBasicValues(){
			$this->sku = $this->testInput($_POST["SKU"]);
			$this->name = $this->testInput($_POST["Name"]);
			$this->price = $this->testInput($_POST["Price"]);
		}

		//Inserts the basic product data into the database
		public function insertProduct(){
			$mysqli = new mysqli('127.0.0.1', 'root', '', '');
			if ($mysqli->connect_error) {
    			die('Connect Error (' . $mysqli->connect_errno . ') '. $mysqli->connect_error);
			}
			$mysqli->select_db("Products");
			$query = "INSERT INTO Product (SKU, Name, Price, Type_ID) VALUES (
				'".$mysqli->real_escape_string($_POST["SKU"])."',
				'".$mysqli->real_escape_string($_POST["Name"])."',
				'".$mysqli->real_escape_string($_POST["Price"])."',
				'".$mysqli->real_escape_string($_POST["Type"])."'
			)";
			$mysqli->query($query);
			$mysqli->close();
		}

		//Checks if entered SKU does not already exist in the database
		public function checkSku(){
			$mysqli = new mysqli('127.0.0.1', 'root', '', '');
			if ($mysqli->connect_error) {
    			die('Connect Error (' . $mysqli->connect_errno . ') '. $mysqli->connect_error);
			}
			$mysqli->select_db("Products");
			$query = "SELECT * FROM Product WHERE SKU = '".$mysqli->real_escape_string($_POST["SKU"])."'";
			$result=$mysqli->query($query);
			if($result->num_rows != 0){
				$mysqli->close();
				return false;
			}
			else{
				$mysqli->close();
				return true;
			}
		}

		public function getSku(){
			return $this->sku;
		}

		public function getName(){
			return $this->name;
		}

		public function getPrice(){
			return $this->price;
		}
	}

	class dvd extends product{
		private $size;

		//Reads size from the form
		public function setSize(){
			$this->size = $this->testInput($_POST["Size"]);
		}

		//Inserts the DVD size into the database
		public function insertAttributes(){
			$mysqli = new mysqli('127.0.0.1', 'root', '', '');
			if ($mysqli->connect_error) {
    			die('Connect Error (' . $mysqli->connect_errno . ') '. $mysqli->connect_error);
			}
			$mysqli->select_db("Products");
			$query = "INSERT INTO Size (Product_SKU, Size) VALUES (
				'".$mysqli->real_escape_string($_POST["SKU"])."',
				'".$mysqli->real_escape_string($_POST["Size"])."'
			)";
			$mysqli->query($query);
			$mysqli->close();
		}

		public function getSize(){
			return $this->size;
		}
	}

	class book extends product{
		private $weight;

		//Reads weight from the form
		public function setWeight(){
			$this->weight = $this->testInput($_POST["Weight"]);
		}

		//Inserts the book's weight into the database
		public function insertAttributes(){
			$mysqli = new mysqli('127.0.0.1', 'root', '', '');
			if ($mysqli->connect_error) {
    			die('Connect Error (' . $mysqli->connect_errno . ') '. $mysqli->connect_error);
			}
			$mysqli->select_db("Products");
			$query = "INSERT INTO Weight (Product_SKU, Weight) VALUES (
				'".$mysqli->real_escape_string($_POST["SKU"])."',
				'".$mysqli->real_escape_string($_POST["Weight"])."'
			)";
			$mysqli->query($query);
			$mysqli->close();
		}

		public function getWeight(){
			return $this->weight;
		}
	}

	class furniture extends product{
		private $height;
		private $width;
		private $length;

		//Reads dimensions from the form
		public function setDimensions(){
			$this->height = $this->testInput($_POST["Height"]);
			$this->width = $this ->testInput($_POST["Width"]);
			$this->length = $this->testInput($_POST["Length"]);
		}

		//Inserts the dimensions into the database
		public function insertAttributes(){
			$mysqli = new mysqli('127.0.0.1', 'root', '', '');
			if ($mysqli->connect_error) {
    			die('Connect Error (' . $mysqli->connect_errno . ') '. $mysqli->connect_error);
			}
			$mysqli->select_db("Products");
			$query = "INSERT INTO Dimensions (Product_SKU, Height, Width, Length) VALUES (
				'".$mysqli->real_escape_string($_POST["SKU"])."',
				'".$mysqli->real_escape_string($_POST["Height"])."',
				'".$mysqli->real_escape_string($_POST["Width"])."',
				'".$mysqli->real_escape_string($_POST["Length"])."'
			)";
			$mysqli->query($query);
			$mysqli->close();
		}

		public function getDimensions(){
			$dimensions = array();
			foreach($this as $key => $value){
				array_push($dimensions, $value);
			}
			return $dimensions;
		}
	}

	//Flag to check if form has validated. "true" = validated, "false" = not validated(error)
	$flag = true;

	//Creates object based on the type selected
	if($_SERVER["REQUEST_METHOD"] == "POST"){
		$product;
		$type = $_POST["Type"];
		switch($type){
			case "1":
			$product = new dvd;
			$product->setSize();
			break;
			case "2":
			$product = new book;
			$product->setWeight();
			break;
			case "3":
			$product = new furniture;
			$product->setDimensions();
			break;
			default: $flag = false;
			break;
		}
		$product->setBasicValues();
	}

?>

<!DOCTYPE html>
<html lang="lv">
	<head>
		<title>Product New</title>
		<meta charset="UTF-8">
		<script src="/script.js"></script>
		<link rel="stylesheet" type="text/css" href="/style.css">
	</head>
	<body>
		<div id="form">
			<h1>Product Add</h1>
			<div class="productadd">
				<form name="addProduct" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" onsubmit="return validate()">
					<input type="submit" value="Save" /><br>
					SKU <input type="text" name="SKU" id="SKU">
					<div class="err">
						<?php
							//Validates SKU
							if($_SERVER["REQUEST_METHOD"] == "POST"){
								if($product->getSku() == ""){
									echo "<p>Please provide a stock keeping unit code</p>";
									$flag = false;
								}
								else{
									if($product->checkSku() == false){
										echo "<p>This stock keeping unit code already exists</p>";
										$flag = false;
									}
								}
							}
						?>
					</div>
					<br>
					Name <input type="text" name="Name" id="Name">
					<div class="err">
						<?php
						//Validates Name
							if($_SERVER["REQUEST_METHOD"] == "POST"){
								if($product->getName() == ""){
									echo "<p>Please provide a product name</p>";
									$flag = false;
								}
							}
						?>
					</div>
					<br>
					Price <input type="text" name="Price" id="Price">
					<div class="err">
						<?php
						//Validates Price
							if($_SERVER["REQUEST_METHOD"] == "POST"){
								if($product->getPrice() == ""){
									echo "<p>Please provide the price of the product</p>";
									$flag = false;
								}
								else{
									//Only decimal format is allowed since we are dealing with money
									$pattern = "/^\d{1,8}\.\d{2}$/";
									if(!preg_match($pattern, $product->getPrice())){
										echo "<p>Please provide the price in a decimal format, for example, \"1.99\"</p>";
										$flag = false;
									}
								}
							}
						?>
					</div>
					<br>
					Type Switcher
					<select id="type" name="Type" onchange="selectType()">
						<option value="1">DVD</option>
						<option value="2">Book</option>
						<option value="3">Furniture</option>
					</select>
					<br>
					<div id="attributes">

					</div>
					<div class="err">
						<?php
						//Validates product's attributes
						if($_SERVER["REQUEST_METHOD"] == "POST"){
							switch($_POST["Type"]){
								case "1":
								if($product->getSize() == ""){
									echo "<p>Please provide DVD size in MB</p>";
									$flag = false;
								}
								else{
									$pattern = "/^\d{1,}$/";
									if(!preg_match($pattern, $product->getSize())){
										echo "<p>Please provide DVD size as an integer</p>";
										$flag = false;
									}
								}
								break;
								case "2":
								if($product->getWeight() == ""){
									echo "<p>Please provide book's weight in kg</p>";
									$flag = false;
								}
								else{
									if(!is_numeric($product->getWeight())){
										echo "<p>Please provide book's weight as a number</p>";
										$flag = false;
									}
								}
								break;
								case "3":
								foreach($product->getDimensions() as $dimension){
									if($dimension == ""){
										echo "<p>Please check if all dimensions have been entered</p>";
										$flag = false;
										break;
									}
									$pattern = "/^\d{1,}$/";
									if(!preg_match($pattern, $dimension)){
										echo "<p>Please provide furniture's dimensions as integers</p>";
										$flag = false;
										break;
									}
								}
								break;
								default: break;
							}
						}
						?>
					</div>
				</form>
			</div>
		</div>
		<button type="button" onclick="productListView()">List all products</button>
	</body>
	<script>
		window.onload = function(){
			selectType();
		}
	</script>
</html>


<?php	
	//Upon successful validation, product's data is entered in the database
	if($_SERVER["REQUEST_METHOD"] == "POST" && $flag == true){
		$product->insertProduct();
		$product->insertAttributes();
	} 
?>