function productAddView(){
	window.location.href = "/product/new";
}
function productListView(){
	window.location.href = "/product/list";
}
//Dynamically changes the attribute field depending on the selected product type
function selectType(){
	var type = document.getElementById("type").value;
	var div = document.getElementById("attributes");
	switch(type){
		case "1":
		div.innerHTML = "Size <input type=\"text\" name=\"Size\" id=\"Size\"><br><div class=\"err\"></div>";
		break;
		case "2":
		div.innerHTML = "Weight <input type=\"text\" name=\"Weight\" id=\"Weight\"><br><div class=\"err\"></div>";
		break;
		case "3":
		div.innerHTML = "Dimensions <br> Height <input type=\"text\" name=\"Height\" id=\"Height\"><br><div class=\"err\"></div> Width <input type=\"text\" name=\"Width\" id=\"Width\"><br><div class=\"err\"></div> Length <input type=\"text\" name=\"Length\" id=\"Length\"><br><div class=\"err\"></div>";
		break;
	}
}
//Client side validation
function validate(){
	var sku = document.forms["addProduct"]["SKU"].value;
	var name = document.forms["addProduct"]["Name"].value;
	var price = document.forms["addProduct"]["Price"].value;
	var type = document.forms["addProduct"]["Type"].value;
	var error = document.getElementsByClassName("err");
	//In case of an error, flag is set to false and the form is not submitted
	var flag = true;

	if(sku == ""){
		error[0].innerHTML = "<p>Please provide a stock keeping unit code</p>";
		flag = false;
	}
	else{
		error[0].innerHTML = "";
	}

	if(name == ""){
		error[1].innerHTML = "<p>Please provide a product name</p>";
		flag = false;
	}
	else{
		error[1].innerHTML = "";
	}

	if(price == ""){
		error[2].innerHTML = "<p>Please provide the price of the product</p>";
		flag = false;
	}
	else{
		//Entered price is tested against regex pattern (1 to 8 digits for euros and 2 digits after decimal point for cents)
		//This can be improved to allow other formats
		var pattern = /^\d{1,8}\.\d{2}$/;
		var result = price.match(pattern);
		if(result == null){
			if(price.length>10){
				error[2].innerHTML = "<p>The entered string was too long</p>";
			}
			else{
				error[2].innerHTML = "<p>Please provide the price in a decimal format, for example, \"1.99\"</p>";
			}
			flag = false;
		}
		else{
			error[2].innerHTML = "";
		}
	}

	switch(type){
		case "1":
			var size = document.forms["addProduct"]["Size"].value;
			if(size == ""){
				error[3].innerHTML = "<p>Please provide DVD size in MB</p>";
				flag = false;
			}
			else{
				var pattern = /^\d{1,}$/;
				var result = size.match(pattern);
				if(result == null){
					error[3].innerHTML = "<p>Please provide DVD size as an integer</p>";
					flag = false;
				}
				else{
					error[3].innerHTML = "";
				}
			}
			break;
		case "2":
			var weight = document.forms["addProduct"]["Weight"].value;
			if(weight == ""){
				error[3].innerHTML = "<p>Please provide book's weight in kg</p>";
				flag = false;
			}
			else{
				var result = Number(weight);
				if(isNaN(result)){
					error[3].innerHTML = "<p>Please provide book's weight as a number</p>";
					flag = false;
				}
				else{
					error[3].innerHTML = "";
				}
			}
			break;
		case "3":
			var height = document.forms["addProduct"]["Height"].value;
			var width = document.forms["addProduct"]["Width"].value;
			var length = document.forms["addProduct"]["Length"].value;
			if(height == ""){
				error[3].innerHTML = "<p>Please provide furniture's height in cm</p>";
				flag = false;
			}
			else{
				var pattern = /^\d{1,}$/;
				var result = height.match(pattern);
				if(result == null){
					error[3].innerHTML = "<p>Please provide furniture's height as an integer</p>";
					flag = false;
				}
				else{
					error[3].innerHTML = "";
				}
			}
			if(width == ""){
				error[4].innerHTML = "<p>Please provide furniture's width in cm</p>";
				flag = false;
			}
			else{
				var pattern = /^\d{1,}$/;
				var result = width.match(pattern);
				if(result == null){
					error[4].innerHTML = "<p>Please provide furniture's width as an integer</p>";
					flag = false;
				}
				else{
					error[4].innerHTML = "";
				}
			}
			if(length == ""){
				error[5].innerHTML = "<p>Please provide furniture's length in cm</p>";
				flag = false;
			}
			else{
				var pattern = /^\d{1,}$/;
				var result = length.match(pattern);
				if(result == null){
					error[5].innerHTML = "<p>Please provide furniture's length as an integer</p>";
					flag = false;
				}
				else{
					error[5].innerHTML = "";
				}
			}
			break;
	}

	return flag;
}