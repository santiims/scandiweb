<?php

//The data here should be changed to an admin account with all necessary mysql privileges
$mysqli = new mysqli('127.0.0.1', 'root', '', '');

if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

//If database does not exist on the server, it is then created
if($mysqli->select_db("Products") === false){
	$query = "CREATE DATABASE IF NOT EXISTS Products";
	$mysqli->query($query);
	$mysqli->select_db("Products");
	//Creation of necessary tables in the database
	$create_type_table =		"CREATE TABLE Type(
									ID int NOT NULL AUTO_INCREMENT PRIMARY KEY,
									Name varchar(20) NOT NULL
								)";
	$create_product_table = 	"CREATE TABLE Product (
									SKU char(10) NOT NULL PRIMARY KEY,
									Name varchar(50) NOT NULL,
									Price decimal(10,2) NOT NULL,
									Type_ID int NOT NULL,
									FOREIGN KEY (Type_ID) REFERENCES Type(ID)
								)";
	$create_size_table = 		"CREATE TABLE Size(
									Product_SKU char(10) NOT NULL,
									Size int NOT NULL,
									FOREIGN KEY (Product_SKU) REFERENCES Product(SKU)
								)";
	$create_weight_table =		"CREATE TABLE Weight(
									Product_SKU char(10) NOT NULL,
									Weight float NOT NULL,
									FOREIGN KEY (Product_SKU) REFERENCES Product(SKU)
								)";
	$create_dimensions_table = 	"CREATE TABLE Dimensions(
									Product_SKU char(10) NOT NULL,
									Height int NOT NULL,
									Width int NOT NULL,
									Length int NOT NULL,
									FOREIGN KEY (Product_SKU) REFERENCES Product(SKU)
								)";
	$mysqli->query($create_type_table);
	$mysqli->query($create_product_table);
	$mysqli->query($create_size_table);
	$mysqli->query($create_weight_table);
	$mysqli->query($create_dimensions_table);

	$types = array("DVD", "Book", "Furniture");

	foreach($types as $type){
		$query = "INSERT INTO Type (Name) VALUES ('".$type."')";
		$mysqli->query($query);
	}

}

$mysqli->close();

header("Location: /product/list");