This is my submission to the test for the position of Junior Web Developer at Scandiweb.

Author: Edgars Lauks

Instructions.
1) In all files named "index.php":
	
	$mysqli = new mysqli('127.0.0.1', 'root', '', '');

	In this line of code, the IP address('127.0.0.1'), MySQL account name ('root') and it's password (''), should be replaced with the server's address and MySQL account's credentials with sufficient priviliges to add and edit databases.

2) Running the code located at "scandiweb/index.php" will automatically create a database and the necessary tables for the rest of the page to function. This needs to be done only once.

3) Naturally, the database will be empty. After the page redirects to "Product List" view, please click the button "Add a product" to start entering data in the database.

Additional notes:
CSS still under construction.

Test environment:
-wampserver 3.1.7
-PHP 7.1.26
-MySQL 5.7.24